import json
from pprint import pprint
import os

setting = json.load(open('settings.json'))

Access = setting['token_access']
Url = setting['url']
OriginDir = os.system('pwd')

def projectScan(idGroup, pathGroup):
    os.system('curl https://gitlab.com/api/v4/groups/'+str(idGroup)+'/?private_token='+Access+' > json/group-'+str(idGroup)+'.json')

    dataGroup = json.load(open('json/group-'+str(idGroup)+'.json'))

    for u in range(len(dataGroup['projects'])):
        pprint(pathGroup + ' - ' + dataGroup['projects'][u]['name'])
        projId = dataGroup['projects'][u]['id']
        projName = dataGroup['projects'][u]['path']
        directory = 'gitlab-repo/'+pathGroup+'/'+projName+'_'+str(projId)

        if not os.path.exists(directory):
            os.system('mkdir ' + directory)
            os.system('git clone https://oauth2:'+ Access +'@gitlab.com/'+pathGroup+'/'+projName +' '+ directory)
        else:
            print(' exist !')


os.system('curl https://gitlab.com/api/v4/groups?private_token='+Access+'> json/groups.json')

data = json.load(open('json/groups.json'))
lenProj = len(data)


for i in range(lenProj):
    print('-----------------------------------------------------------')
    directory = 'gitlab-repo/'+data[i]['path']
    if not os.path.exists(directory):
        os.system('mkdir ' + directory)
    else:
        print(directory + ' exist !')
    projectScan(data[i]['id'], data[i]['full_path'])
